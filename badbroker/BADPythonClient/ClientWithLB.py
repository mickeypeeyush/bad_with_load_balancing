import badclient
from badclient import MQ
import sys
import time
import logging
import configparser

from CommandListener import CommandListener

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


# Added By Peeyush

config = configparser.ConfigParser()
config.read(sys.argv[1])

BROKER_DISCOVERY_IP = config.get("Client", "bcsIP") #"localhost"
BROKER_DISCOVERY_PORT = config.get("Client", "bcsPort") #"5000"
BROKER_DISCOVERY_PATH = config.get("Client", "strategy") #"/getNextBrokerRR"
CLIENT_USER = config.get("Client", "user") #"test"
CLIENT_PASS = config.get("Client", "pass") #"test"
CLIENT_EMAIL = config.get("Client", "email") #"test@test.edu"
CLIENT_DATAVERSE = config.get("Client", "dataverse") #"test"
CLIENT_DATASET = config.get("Client", "dataset") #"test"
CLIENT_PORT =  config.get("Client", "port")


def on_channelresults(channelName, subscriptionId, results):
    print('Retrieved results for channel `%s` with sub `%s` -- %d records' %(channelName, subscriptionId, len(results)))
    for item in results:
        print('APPDATA ' + str(item))
    return True


def on_error(where, error_msg):
    print(where, ' ---> ', error_msg)


def connectToBroker():
    client = badclient.BADClient(BROKER_DISCOVERY_IP, BROKER_DISCOVERY_PORT, BROKER_DISCOVERY_PATH, CLIENT_PORT)
    dataverseName = CLIENT_DATAVERSE
    userName = CLIENT_USER
    password = CLIENT_PASS
    email = CLIENT_EMAIL

    print ("Connected To Broker %s %s" % (client.brokerServer, client.brokerPort))

    client.on_channelresults = on_channelresults
    client.on_error = on_error

    client.register(dataverseName, userName, password, email)
    subIds = []

    if client.login():
        client.listchannels()
        subIds = client.listsubscriptions()
        #print(subIds)
        return client
    else:
        print('Registration or Login failed')
        sys.exit(0)

if __name__ == "__main__":

    client = None
    try:
        client = connectToBroker()
        #client.run()  # blocking call
        messagingThread = MQ(client)
        messagingThread.start()
        cl = CommandListener(client, CLIENT_PORT)
        cl.start()

        time.sleep(1)
        while True:
            print("\n\n")
            print("--------Client Commands Window----------")
            print("1 --> Login")
            print("2 --> Logout")
            print("3 --> List Channels")
            print("4 --> List Subscriptions")
            print("5 --> Subscribe")
            print("6 --> Unsubscribe")
            print("7 --> Get Results")

            print(">>")
            query = input()
            if query == "1":
                client.login()
            elif query == "2":
                client.logout()
            elif query == "3":
                client.listchannels()
            elif query == "4":
                client.listsubscriptions()
            elif query == "5":
                channel = input()
                params = input().split()
                print("Subscriptin ID", client.subscribe(channel, params))
            elif query == "6":
                subID = input()
                client.unsubcribe(subID)
            elif query == "7":
                subID = input()
                client.getresults(subID,10)


    except KeyboardInterrupt:
        client.stop()
