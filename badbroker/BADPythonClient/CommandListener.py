from flask import Flask, jsonify, redirect
from flask import request
import json
from threading import Thread
import logging

app = Flask(__name__)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)

# Added By Peeyush

class CommandListener(Thread):

    def __init__(self, client, port):
        super().__init__()
        self.client = client
        self.port = port

    def run(self):
        #print("Here")
        #self.app.run(host="0.0.0.0", port=self.port, debug=False, use_reloader=False)

        @app.route("/reconnect", methods=['POST'])
        def updateLoadInfo():
            try:
                result = request.get_json()
                brokerIP = result['brokerIP']
                brokerPort = result['brokerPort']
                self.client.reconnect(brokerIP, brokerPort)
                log.info("Reconnected To New Broker {}:{} Successfully.".format(brokerIP, brokerPort))
                return "Reconnected To New Broker Successfully."
            except FloatingPointError:
                return "Badly formatted request."

        app.run(host="0.0.0.0", port=self.port, debug=False, use_reloader=False)