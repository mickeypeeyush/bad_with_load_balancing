from IPtoLatLong import IPtoLatLong
from StaticLB import StaticLoadBalancer, RandomLoadBalancer, RoundRobinLoadBalancer, NearestLoadBalancer
from CachedData import CachedData
from OverloadDetector import OverloadDetectorService

from flask import Flask, jsonify, redirect
from flask import request
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# Updated By Peeyush

@app.route("/getNextBroker")
def getNextBroker():
    broker = StaticLoadBalancer().getNext()
    if broker:
        return jsonify({'brokerServer': broker.ip,
                        'brokerPort': broker.port})
    else:
        return "Error. No brokers registered."


@app.route("/getNextBrokerRR")
def getNextBrokerRR():
    broker = RoundRobinLoadBalancer().getNext()
    if broker:
        return jsonify({'brokerServer': broker.ip,
                        'brokerPort': broker.port})
    else:
        return "Error. No brokers registered."


@app.route("/getNextBrokerNearest")
def getNextBrokerNearest():
    clientIP = str(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
    broker = NearestLoadBalancer().getNext(clientIP)
    if broker:
        return jsonify({'brokerServer': broker.ip,
                        'brokerPort': broker.port})
    else:
        return "Error. No brokers registered."


@app.route("/getNextBrokerRandom")
def getNextBrokerRandom():
    broker = RandomLoadBalancer().getNext()
    if broker:
        return jsonify({'brokerServer': broker.ip,
                        'brokerPort': broker.port})
    else:
        return "Error. No brokers registered."


@app.route("/registerbroker", methods=['POST'])
def registerBroker():
    try:
        result = request.get_json()
        brokerName = result['brokerName']
        brokerIP = result['brokerIP']
        brokerPort = result['brokerPort']
        CachedData.getInstance().addNewBroker(brokerName, brokerIP, brokerPort)
        #print(CachedData.getInstance().brokers)
        return "Added broker successfully."
    except:
        return "Badly formatted request. Ke ys must be brokerName and brokerIP"


@app.route("/loadinfo", methods=['POST'])
def updateLoadInfo():
    try:
        result = request.get_json()
        brokerName = result['brokerName']
        brokerIP = result['brokerIP']
        brokerPort = result['brokerPort']
        brokerLoad = result['brokerLoad']
        CachedData.getInstance().updateLoadInfo(brokerName, brokerIP, brokerPort, brokerLoad)
        return "Updated Load successfully."
    except:
        return "Badly formatted request."


@app.route("/showinfo")
def showInfo():
    try:
        #print(json.dumps(CachedData.getInstance().brokers, default=lambda o: o.__dict__))
        return json.dumps(CachedData.getInstance().brokers, default=lambda o: o.__dict__)
    except:
        return "{'Badly formatted request.': 'failed'}"

if __name__ == "__main__":
    overloadDetector = OverloadDetectorService()
    overloadDetector.start()
    app.run(host='0.0.0.0', debug=True)
