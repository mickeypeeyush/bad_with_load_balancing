from threading import Thread
from common import utils
import requests

""" Class balancing load after the detector service has detected
an overloaded and underloaded broker.

Involves offloading channels, sessions and subscriptions from overloaded broker
 and pushing it to underloaded broker
"""

# Added By Peeyush

class DynamicLoadBalancer(Thread):

    def __init__(self, olBroker, ulBroker):
        super().__init__()
        self.olBroker = olBroker
        self.ulBroker = ulBroker

    def run(self):
        self.balance()

    def balance(self):
        data = {
            'brokerServer': self.ulBroker.ip,
            'brokerPort': self.ulBroker.port
        }
        requests.post(utils.createBrokerURL(self.olBroker, "/balance"), json=data)

