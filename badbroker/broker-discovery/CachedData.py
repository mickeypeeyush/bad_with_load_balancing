from BrokerInfo import BrokerInfo
from IPtoLatLong import IPtoLatLong
from common.LoadInfo import LoadInfo

# Added By Peeyush
NUM_SUBSCRIBERS_WEIGHT = 2
NUM_SUBSCRIPTIONS_WEIGHT = 5
NUM_CHANNELS_WEIGHT = 0.2
CPU_WEIGHT = 0.1
MEM_WEIGHT = 0.4
MESSAGE_WEIGHT = 0.03

class CachedData(object):

    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = CachedData()

        return cls.instance

    def __init__(self):
        self.brokers = []
        self.roundRobinIndex = -1

    def addNewBroker(self, brokerName, brokerIP, brokerPort):
        for broker in self.brokers:
            if brokerName == broker.name:
                broker.ip = brokerIP
                broker.port = brokerPort
                return

        brokerInfo = BrokerInfo()
        brokerInfo.ip = brokerIP
        brokerInfo.name = brokerName
        brokerInfo.port = brokerPort
        try:
            brokerInfo.latitude, brokerInfo.longitude = IPtoLatLong.getInstance().getLatLong(brokerIP)
        except:
            pass

        brokerInfo.load = LoadInfo()
        self.brokers.append(brokerInfo)

    def updateLoadInfo(self, brokerName, brokerIP, brokerPort, brokerLoad):
        for broker in self.brokers:
            if brokerName == broker.name:
                broker.load = LoadInfo(brokerLoad)
                self.calculateScore(broker)
                break
        else:
            brokerInfo = BrokerInfo()
            brokerInfo.ip = brokerIP
            brokerInfo.name = brokerName
            brokerInfo.port = brokerPort
            try:
                brokerInfo.latitude, brokerInfo.longitude = IPtoLatLong.getInstance().getLatLong(brokerIP)
            except:
                pass

            brokerInfo.load = LoadInfo()

            self.brokers.append(brokerInfo)

    def calculateScore(self, broker):
        broker.loadScore = ((NUM_SUBSCRIBERS_WEIGHT*broker.load.userSubscriptions)
                                + (NUM_SUBSCRIBERS_WEIGHT*broker.load.sessions)
                                + (CPU_WEIGHT*broker.load.cpuUsage)
                                + (MEM_WEIGHT*broker.load.memoryUsage)
                                + (MESSAGE_WEIGHT*broker.load.messageQueueLength))