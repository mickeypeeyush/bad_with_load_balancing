import maxminddb
from math import radians, cos, sin, asin, sqrt
import sys


class IPtoLatLong(object):
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = IPtoLatLong()
        return cls.instance

    def __init__(self, GeoIPdatabaseName='GeoLite2-City.mmdb'):
        try:
            self.reader = maxminddb.open_database(GeoIPdatabaseName)
        except:
            print("Maxmind IP to Lat Long database not present in path.")
        self.brokers=[]

    def getLatLong(self, ipAddress):
        try:
            return (self.reader.get(ipAddress)['location']['latitude'],self.reader.get(ipAddress)['location']['longitude'])
        except:
            return (23.2, 45.7)


    def haversine(self,lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # Flicked from a Stack Overflow answer at http://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        r = 6371 # Radius of earth in kilometers. Use 3956 for miles
        return c * r

if __name__=="__main__":
    reg=IPtoLatLong()
    reg.registerBroker('abc','12.23.44.11')
    print(reg.getNearestBroker('127.0.0.0').IP)
