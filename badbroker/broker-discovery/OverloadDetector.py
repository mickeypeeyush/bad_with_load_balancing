import time
from threading import Thread
from CachedData import CachedData
from DynamicLB import DynamicLoadBalancer

# Added By HANG

OVERLOAD_THRESHOLD = 55
UNDERLOAD_THRESHOLD = 1
CPU_OVERLOAD = 90
MEM_OVERLOAD = 90
SUBSBSCRIBERS_OVERLOAD = 1000
SUBSCRIPTIONS_OVERLOAD = 10000
MESSAGE_OVERLOAD = 100

NUM_SUBSCRIBERS_WEIGHT = 2
NUM_SUBSCRIPTIONS_WEIGHT = 5
NUM_CHANNELS_WEIGHT = 0.2
CPU_WEIGHT = 0.1
MEM_WEIGHT = 0.4
MESSAGE_WEIGHT = 0.03


class OverloadDetectorService(Thread):

    def __init__(self):
        super().__init__()
        self.data = CachedData.getInstance().brokers
        self.dynamicLB = None

    def run(self):
        """Using the cached load data figure out which brokers are
         overloaded and which brokers are underloaded.
        """
        time.sleep(20)
        while True:
            time.sleep(5)
            brokers = self.data
            scores = []

            for broker in brokers:
                scores.append([broker, self.calculateOverloadScore(broker)])

            if not scores:
                continue

            scores = sorted(scores, key=lambda x: x[1])
            if self.isOverloaded(scores[-1][1]):
                olBroker = scores[-1][0]
                ulBroker = scores[0][0]
                print("Overload detected on Broker {} {}:{}".format(olBroker.name, olBroker.ip,
                                                                     olBroker.port))
                print("Replacement Broker is {} {}:{}".format(ulBroker.name, ulBroker.ip,
                                                                     ulBroker.port))
                dynamicLB = DynamicLoadBalancer(olBroker, ulBroker)
                dynamicLB.run()
                time.sleep(20)
                continue

            for broker in brokers:
                if self.isMetricOverloaded(broker):
                    olBroker = broker
                    ulBroker = scores[0][0]
                    print("Overload detected on Broker {} {}:{}".format(olBroker.name, olBroker.ip,
                                                                     olBroker.port))
                    print("Replacement Broker is {} {}:{}".format(ulBroker.name, ulBroker.ip,
                                                                     ulBroker.port))
                    dynamicLB = DynamicLoadBalancer(olBroker, ulBroker)
                    dynamicLB.run()
                    time.sleep(20)
                    break

    def calculateOverloadScore(self, broker):
        broker.loadScore = ((NUM_SUBSCRIBERS_WEIGHT*broker.load.userSubscriptions)
                                + (NUM_SUBSCRIBERS_WEIGHT*broker.load.sessions)
                                + (CPU_WEIGHT*broker.load.cpuUsage)
                                + (MEM_WEIGHT*broker.load.memoryUsage)
                                + (MESSAGE_WEIGHT*broker.load.messageQueueLength))

        return broker.loadScore

    def isOverloaded(self, score):
        if score >= OVERLOAD_THRESHOLD:
            return True
        return False

    def isUnderLoaded(self, score):
        if score <= UNDERLOAD_THRESHOLD:
            return True
        return False

    def isMetricOverloaded(self, broker):
        if broker.load.userSubscriptions >= SUBSCRIPTIONS_OVERLOAD:
            return True
        if broker.load.sessions >= SUBSBSCRIBERS_OVERLOAD:
            return True
        if broker.load.cpuUsage >= CPU_OVERLOAD:
            return True
        if broker.load.memoryUsage >= MEM_OVERLOAD:
            return True
        if broker.load.messageQueueLength >= MESSAGE_OVERLOAD:
            return True

        return False
