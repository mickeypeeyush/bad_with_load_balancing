from CachedData import CachedData
from IPtoLatLong import IPtoLatLong
import sys
import random

NUM_SUBSCRIBERS_WEIGHT = 2
NUM_SUBSCRIPTIONS_WEIGHT = 5
NUM_CHANNELS_WEIGHT = 0.2
CPU_WEIGHT = 0.1
MEM_WEIGHT = 0.4
MESSAGE_WEIGHT = 0.03


# Added By HANG


class StaticLoadBalancer(object):

    def __init__(self):
        pass

    def getNext(self):
        min_ = -1
        broker = None
        self.updateAllScores()

        for v in CachedData.getInstance().brokers:
            if min_ == -1 or min_ > v.loadScore:
                broker = v
                min_ = v.loadScore

        return broker

    def updateAllScores(self):
        for broker in  CachedData.getInstance().brokers:
            self.calculateScore(broker)

    def calculateScore(self, broker):
        broker.loadScore = ((NUM_SUBSCRIBERS_WEIGHT*broker.load.userSubscriptions)
                                + (NUM_SUBSCRIBERS_WEIGHT*broker.load.sessions)
                                + (CPU_WEIGHT*broker.load.cpuUsage)
                                + (MEM_WEIGHT*broker.load.memoryUsage)
                                + (MESSAGE_WEIGHT*broker.load.messageQueueLength))


class RoundRobinLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        nextIndex = ((CachedData.getInstance().roundRobinIndex + 1) % len(CachedData.getInstance().brokers))
        CachedData.getInstance().roundRobinIndex = nextIndex
        return CachedData.getInstance().brokers[nextIndex]


class RandomLoadBalancer(StaticLoadBalancer):

    def getNext(self):
        nextIndex = random.randint(0, len(CachedData.getInstance().brokers))
        return CachedData.getInstance().brokers[nextIndex]


class NearestLoadBalancer(StaticLoadBalancer):

    def getNext(self, clientIP):
        minDistance = sys.maxsize
        (clientLatitude, clientLongitude) = IPtoLatLong.getInstance().getLatLong(clientIP)
        brokers = CachedData.getInstance().brokers

        if (not clientLatitude or not clientLongitude):
            return brokers[0]

        nearestBroker = brokers[0]

        for broker in brokers:
            distanceToCurrentBroker = IPtoLatLong.getInstance().haversine(
                clientLongitude, clientLatitude, broker.Longitude, broker.Latitude)
            if (distanceToCurrentBroker < minDistance):
                minDistance=distanceToCurrentBroker
                nearestBroker=broker

        return nearestBroker
