from CachedData import CachedData
import pprint
import json
import requests

BROKER_DISCOVERY_IP = "localhost"
BROKER_DISCOVERY_PORT = "5000"

# Added By Peeyush


class Admin(object):

    def __init__(self):
        self.discoveryUrl = 'http://{}:{}'.format(BROKER_DISCOVERY_IP, BROKER_DISCOVERY_PORT)

    def showBrokerInfo(self):

        r = requests.get(self.discoveryUrl + '/showinfo')

        if r.status_code == 200:
            response = r.json()
            print(json.dumps(response, indent=4))
            #pprint.pprint(response)
        else:
            print({})


if __name__ == "__main__":
    admin = Admin()
    while True:
        print(">>")
        line = input()
        if line == "show info":
            admin.showBrokerInfo()
        print()

