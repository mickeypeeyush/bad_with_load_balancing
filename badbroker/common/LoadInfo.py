import json

# Added By Peeyush


class LoadInfo(object):

    def __init__(self, jsonData=None):
        if jsonData is not None:
            self.__dict__ = json.loads(jsonData)
        else:
            self.cpuUsage = 0
            self.memoryUsage = 0
            self.messageQueueLength = 0
            self.sessions = 0
            #self.channelSubscriptions = 0
            self.userSubscriptions = 0