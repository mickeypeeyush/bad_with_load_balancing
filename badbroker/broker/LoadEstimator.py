from threading import Thread
import time
import psutil

from common.LoadInfo import LoadInfo

# Added By Peeyush


class LoadEstimatorService(object):

    def __init__(self, broker):
        self.broker = broker
        self.load = LoadInfo()
        self.updateLoadInfo()

    def updateLoadInfo(self):
        self.load.cpuUsage = psutil.cpu_percent()
        self.load.memoryUsage = psutil.virtual_memory().percent
        self.load.messageQueueLength = self.broker.cache.length()

        self.load.sessions = 0
        try:
            for k, v in self.broker.sessions.items():
                self.load.sessions += len(v)
        except:
            pass

        self.load.userSubscriptions = 0
        #print("Load Estimator", self.broker.userToSubscriptionMap)
        try:
            for k, v in self.broker.userToSubscriptionMap.items():
                for k1, v1 in v.items():
                    self.load.userSubscriptions += len(v1)
        except:
            pass