import logging

def setup_logging(name=__name__):
    logging.basicConfig(format='%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s', level=logging.INFO)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    log = logging.getLogger(name)
    return log
