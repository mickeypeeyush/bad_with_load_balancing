from threading import Thread
import time
import requests
import json

import brokerutils
from LoadEstimator import LoadEstimatorService

log = brokerutils.setup_logging(__name__)

# Added By Peeyush


class LoadBalancerService(Thread):

    def __init__(self, broker):
        super().__init__()
        self.broker = broker
        self.loadEstimator = LoadEstimatorService(broker)

    def run(self):
        while True:
            time.sleep(10)
            self.loadEstimator.updateLoadInfo()
            self.communicateLoadInfoWithBDS()


    def communicateLoadInfoWithBDS(self):
        post_request = {
            'brokerName': self.broker.brokerName,
            'brokerIP': self.broker.brokerIPAddr,
            'brokerPort': self.broker.brokerPort,
            'brokerLoad': json.dumps(self.loadEstimator.load.__dict__)
        }
        #log.info(post_request)

        try:
            r = requests.post(self.broker.bcsUrl + "/loadinfo", json=post_request)
            if r and r.status_code == 200:
                #log.info('Load updated successfully')
                log.info(r.text)
                pass
            else:
                log.debug('Broker Load Info Failed!!')
        except Exception as e:
            log.debug('Broker Load Info Failed!! for reason %s' %(e))

