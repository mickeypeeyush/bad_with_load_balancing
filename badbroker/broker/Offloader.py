import random
from threading import Thread
import brokerutils
import requests

log = brokerutils.setup_logging(__name__)

# Added By HANG

class Offloader(Thread):

    def __init__(self, broker, brokerServer, brokerPort):
        super().__init__()
        self.broker = broker
        self.brokerServer = brokerServer
        self.brokerPort = brokerPort

    def run(self):
        log.info("Offloading Clients")
        clients = selectClientsToUnload(self.broker)

        post_request = {
                "brokerIP": str(self.brokerServer),
                "brokerPort": self.brokerPort
        }

        #log.info(post_request)
        #log.info(clients)
        for client in clients:
            print(client)
            try:
                clientUrl = "http://{}:{}".format(client[0], client[1])
                log.info("Offloading Client " + clientUrl)
                r = requests.post(clientUrl + "/reconnect", json=post_request)
                self.broker.clearStateForUser(client[2], client[3])
                if r and r.status_code == 200:
                    log.info('Client unloaded successfully')
                    #log.info(r.text)
                else:
                    log.info('Client unload failed')
            except KeyError as e:
                log.info('Client unload failed for reason %s' %(e))


def selectClientsToUnload(broker):

    for k, v in broker.sessions.items():
        index = random.randint(0, len(v)-1)
        i = 0
        for k1, v1 in v.items():
            if index == i:
                return [(v1.ip, v1.port, k, k1)]
            i += 1


def selectClientsBasedOnSubscription(broker):
    return []